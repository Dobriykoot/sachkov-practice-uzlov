import { writeFileSync } from "fs";
import path from "path";
import { BelongsTo, Column, DataType, ForeignKey, HasMany, Model, Sequelize, Table } from "sequelize-typescript";

// Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

const { PASSWORD_LEN } = process.env;

const task1 = () => {
  if (typeof PASSWORD_LEN !== "string" || PASSWORD_LEN.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент PASSWORD_LEN не был передан" }));
    return;
  }

  const passwordLenNum = Number(PASSWORD_LEN);
  if (isNaN(passwordLenNum)) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент PASSWORD_LEN должем быть числом" }));
    return;
  }

  const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+=-";
  let result = "";
  for (let i = 0; i < passwordLenNum; ++i) {
    result += charset.charAt(Math.floor(Math.random() * charset.length));
  }

  writeFileSync(
    filePathForTask1,
    JSON.stringify({
      input: {
        PASSWORD_LEN,
      },
      result,
    })
  );
};

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, "output", "output2.json");

const initDB = async () => {
  @Table({})
  class Mark extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    lesson: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    fio: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    examDate: string;

    @Column({
      type: DataType.INTEGER,
      allowNull: false,
    })
    mark: number;

    @ForeignKey(() => Student)
    studentId: string;

    @BelongsTo(() => Student, {
      foreignKey: "studentId",
    })
    student!: Student;
  }

  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;

    @HasMany(() => Mark, {
      foreignKey: "studentId",
    })
    markList: Mark[];
  }

  const sequelize = new Sequelize({
    dialect: "postgres",
    host: "database",
    port: 5432,
    username: "postgres",
    password: "qwerty",
    database: "sachkov_practice_db",
  });

  sequelize.addModels([Student, Mark]);

  try {
    await sequelize.authenticate();
    console.log("Init sequelize OK");
  } catch (error) {
    console.error("Init sequelize error", error);
  }

  const resultExamDateList = await Mark.findAll({
    attributes: ["examDate"],
    group: ["examDate"],
  });

  writeFileSync(filePathForTask2, JSON.stringify(resultExamDateList));
};

initDB();
